This repository contains the code associated to the manuscript:

## The impact of genetic diversity on gene essentiality within the E. coli species

François Rousset, José Cabezas Caballero, Florence Piastra-Facon, Jesús Fernández-Rodríguez, Olivier Clermont, Erick Denamur, Eduardo P.C. Rocha & David Bikard

Bacteria from the same species can differ widely in their gene content. In E. coli, the set of genes shared by all strains, known as the core genome, represents about half the number of genes present in any strain. While recent advances in bacterial genomics have enabled to unravel genes required for fitness in various experimental conditions at the genome scale, most studies have focused on model strains. As a result, the impact of this genetic diversity on core processes of the bacterial cell largely remains to be investigated. Here, we developed a new CRISPR interference platform for high-throughput gene repression that is compatible with most E. coli isolates and closely-related species. We applied it to assess the importance of ~3,400 nearly ubiquitous genes in 3 growth media in 18 representative E. coli strains spanning most common phylogroups and lifestyles of the species. Our screens highlighted extensive variations in gene essentiality between strains and conditions. Unlike variations in gene expression level, variations in gene essentiality do not recapitulate the strains’ phylogeny. Investigation of the genetic determinants for these variations highlighted the importance of epistatic interactions with mobile genetic elements. In particular, we showed how mobile genetic elements can trigger the essentiality of core genes that are usually nonessential. This study provides new insights into the evolvability of gene essentiality and argues for the importance of studying various isolates from the same species in bacterial genomics.



Figures from the manuscript can be generated using the jupyter notebook: Notebook.ipynb